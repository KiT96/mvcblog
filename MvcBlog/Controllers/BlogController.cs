﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MvcBlog.DB;
using MvcBlog.Models;

namespace MvcBlog.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class BlogController : Controller
    {

        [HttpGet]
        public async Task<ActionResult<List<BlogModel>>> Index()
        {
            //return Ok(await BlogContext.GetBlogContext().GetList());
            var blogModels = await BlogContext.GetBlogContext().GetList();
            return View(blogModels);
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<ActionResult<BlogModel>> Detail(int Id)
        {
            if (ModelState.IsValid)
            {
                //return Ok(await BlogContext.GetBlogContext().GetById(Id));
                return View(await BlogContext.GetBlogContext().GetById(Id));
            }
            else
            {
                return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
            }
        }
    }
}