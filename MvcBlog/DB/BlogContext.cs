﻿using Microsoft.EntityFrameworkCore;
using MvcBlog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace MvcBlog.DB
{
    public class BlogContext : DbContext
    {
        private static BlogContext _instance;
        private static object _synclock = new object();
        private const string connectionString = "Data Source=HSSSC1PCL01494\\KHANHIT;Initial Catalog=Blog;User ID=sa;Password=Khanhit96";

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder.UseSqlServer(connectionString);
        }

        public DbSet<BlogModel> Blogs { set; get; }

        public static BlogContext GetBlogContext()
        {
            if (_instance == null)
            {
                lock (_synclock)
                {
                    if (_instance == null)
                    {
                        _instance = new BlogContext();
                    }
                }
            }
            return _instance;
        }

        public async Task<List<BlogModel>> GetList()
        {
            try
            {
                return await GetBlogContext().Blogs.ToListAsync();
            }
            catch (SystemException)
            {
                return null;
            }

        }

        public async Task<BlogModel> GetById(int Id)
        {
            try
            {
                return await (from blo in GetBlogContext().Blogs
                              where blo.BlogId == Id
                              select blo).FirstOrDefaultAsync();
            }
            catch (SystemException)
            {
                return null;
            }
        }
    }
}
