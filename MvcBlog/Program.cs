using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using MvcBlog.DB;

namespace MvcBlog
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BlogContext.GetBlogContext().Database.EnsureCreatedAsync();
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
